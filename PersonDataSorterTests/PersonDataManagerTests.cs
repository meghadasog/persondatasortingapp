﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PersonDataSorter;
using Moq;
using System.Collections.Generic;

namespace PersonDataSorterTests
{
    [TestClass]
    public class PersonDataManagerTests
    {
        private PersonDataManager _target;
        private Mock<IDataHandler> _dataHandlerMock;

        [TestInitialize]
        public void Setup()
        {
            _dataHandlerMock = new Mock<IDataHandler>();
            _target = new PersonDataManager(_dataHandlerMock.Object);
        }

        [TestMethod]
        [ExpectedException(typeof(FormatException))]
        public void PersonDataManagerTests_InputDataNotRightFormatTest()
        {
            string fileName = "test.txt";

            _dataHandlerMock.Setup(f => f.ReadInput(fileName)).Throws(new FormatException("Input Data not in right format"));

            _target.SortInputData(fileName);

        }

        [TestMethod]
        public void PersonDataManagerTests_ScoresSameTest()
        {
            var inputData = new List<PersonData>()
            {
                new PersonData { FirstName ="BUNDY", LastName ="TERESSA", Score = 80 },
                new PersonData { FirstName ="SMITH", LastName ="ALLAN", Score = 80 },
                new PersonData { FirstName ="KING", LastName ="MADISON", Score = 80 },
                new PersonData { FirstName ="SMITH", LastName ="FRANCIS", Score = 80 }
            };

            string fileName = "test.txt";

            _dataHandlerMock.Setup(f => f.ReadInput(fileName)).Returns(inputData);

            _target.SortInputData(fileName);
        }


        [TestMethod]
        public void PersonDataManagerTests_ScoresDifferentTest()
        {
            var inputData = new List<PersonData>()
            {
                new PersonData { FirstName ="BUNDY", LastName ="TERESSA", Score = 88 },
                new PersonData { FirstName ="SMITH", LastName ="ALLAN", Score = 70 },
                new PersonData { FirstName ="KING", LastName ="MADISON", Score = 88 },
                new PersonData { FirstName ="SMITH", LastName ="FRANCIS", Score = 85 }
            };

            string fileName = "test.txt";

            _dataHandlerMock.Setup(f => f.ReadInput(fileName)).Returns(inputData);

            _target.SortInputData(fileName);
        }

    }
}
