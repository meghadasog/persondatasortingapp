﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using PersonDataSorter;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PersonDataSorterTests
{
    [TestClass]
    public class DataHandlerTests
    {
        private Mock<IFileManager> _fileManagerMock;
        private DataHandler _target;

        [TestInitialize]
        public void SetUp()
        {
            _fileManagerMock = new Mock<IFileManager>();
            _target = new DataHandler(_fileManagerMock.Object);
        }

        [TestMethod]
        public void DataHandlerTests_InputDataParseTest()
        {
             string fileName = "test.txt";
            var inputData = new List<string>()
            {
                "BUNDY, TERESSA, 88",
                "SMITH, ALLAN, 70",
                "KING, MADISON, 88",
                "SMITH, FRANCIS, 85"
            };

            _fileManagerMock.Setup(f => f.ReadInput(fileName)).Returns(inputData);

            var personsData = _target.ReadInput(fileName);

            Assert.AreEqual(personsData.Count, 4);            
        }
    }
}
