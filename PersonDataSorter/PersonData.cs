﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PersonDataSorter
{
    public class PersonData
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int Score{ get; set; }

        public string ConcatPersonData()
        {
            return FirstName + "," + LastName + "," + Score;
        }
    }
}
