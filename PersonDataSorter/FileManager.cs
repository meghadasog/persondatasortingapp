﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PersonDataSorter
{
    public class FileManager : IFileManager
    {
        public List<string> ReadInput(string fileName)
        {
            if(File.Exists(fileName))
            {
                if (File.ReadAllLines(fileName).Any())
                {
                    return File.ReadAllLines(fileName).ToList();
                }
            }

            throw new FileNotFoundException("Input file not found");
        }

        public void WriteOutput(List<string> personsData, string fileName)
        {
            File.WriteAllLines(fileName, personsData);
        }
    }
}
