﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PersonDataSorter
{
    public class PersonDataManager : IPersonDataManager
    {
        private readonly IDataHandler _dataHandler;

        public PersonDataManager(IDataHandler dataHandler)
        {
            _dataHandler = dataHandler;
        }

        public void SortInputData(string fileName)
        {
            try
            {
                //Get persons data
                var personsData = _dataHandler.ReadInput(fileName);

                var sortedData = personsData;

                //sort data
                if (personsData.Select(pd => pd.Score).Distinct().Count() == 1)
                {
                    //sort by last name, then by first name
                    sortedData = personsData.OrderBy(pd => pd.LastName).ThenBy(pd => pd.FirstName).ToList();
                }
                else
                {
                    //sort by score.
                    sortedData = personsData.OrderBy(pd => pd.Score).ToList();
                }

                //Write to a file
                _dataHandler.WriteSortedData(sortedData);
            }
            catch(FileNotFoundException ex)
            {
                Console.WriteLine("Input file not found." + ex.ToString());
            }
            catch(DataException ex)
            {
                Console.WriteLine("Input data not in right format." + ex.ToString());
            }
            
        }
    }
}
