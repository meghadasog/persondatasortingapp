﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PersonDataSorter
{
    public class DataHandler : IDataHandler
    {
        private const string OUTPUTFILENAME = "grades-sorted.txt";
        private readonly IFileManager _fileManager;

        public DataHandler(IFileManager fileManager)
        {
            _fileManager = fileManager;
        }

        public List<PersonData> ReadInput(string fileName)
        {
            var personsDataRaw = _fileManager.ReadInput(fileName);

            return ParsePersonsData(personsDataRaw);
        }

        public void WriteSortedData(List<PersonData> personsData)
        {
            List<string> personsDataRaw = new List<string>();

            foreach (var personData in personsData)
            {
                personsDataRaw.Add(personData.ConcatPersonData());
            }

            _fileManager.WriteOutput(personsDataRaw, OUTPUTFILENAME);
        }

        private List<PersonData> ParsePersonsData(List<string> personsRawData)
        {
            List<PersonData> personsData = new List<PersonData>();

            foreach (var personRawData in personsRawData)
            {
                string [] rawData = personRawData.Split(','); //Assuming comma seperated string data

                try {

                    personsData.Add(new PersonData { FirstName = rawData[0], LastName = rawData[1],
                        Score = int.Parse(rawData[2]) });
                }
                catch (FormatException formatEx)
                {
                    throw new DataException("Failed to parse Data" + formatEx.ToString());
                }
                catch (IndexOutOfRangeException indexEx)
                {
                    throw new DataException("Failed to parse Data"  + indexEx.ToString());
                }

            }

            return personsData;
        }
    }
}
