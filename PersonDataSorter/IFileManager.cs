﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PersonDataSorter
{
    public interface IFileManager
    {
        List<string> ReadInput(string fileName);
        void WriteOutput(List<string> personsData, string fileName);
    }
}
