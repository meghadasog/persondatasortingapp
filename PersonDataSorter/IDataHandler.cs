﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PersonDataSorter
{
    public interface IDataHandler
    {
        List<PersonData> ReadInput(string fileName);

        void WriteSortedData(List<PersonData> personsData);
    }
}
