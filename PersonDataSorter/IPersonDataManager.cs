﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PersonDataSorter
{
    public interface IPersonDataManager
    {
        void SortInputData(string fileName);
    }
}
