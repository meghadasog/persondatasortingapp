﻿using Microsoft.Practices.Unity;
using PersonDataSorter;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application
{
    class Program
    {
        static void Main(string[] args)
        {
            if (args.Length < 1)
            {
                Console.WriteLine("Not enough command line arguments to run the application.");
            }
            //use unity to create Personal data manager and sort input data.
            UnityContainer container = new UnityContainer();

            container.RegisterType<IPersonDataManager, PersonDataManager>();

            container.RegisterType<IDataHandler, DataHandler>();

            container.RegisterType<IFileManager, FileManager>();

            var personDataManager = container.Resolve<IPersonDataManager>();

            personDataManager.SortInputData(args[0]);
        }
    }
}
